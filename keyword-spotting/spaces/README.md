---
title: Keyword Spotting
emoji: 🏢
colorFrom: gray
colorTo: pink
sdk: gradio
sdk_version: 3.0.20
app_file: app.py
pinned: false
---

Check out the configuration reference at https://huggingface.co/docs/hub/spaces-config-reference
